Nodes	113
Edges	6.2K
Density	0.983249
Maximum degree	112
Minimum degree	85
Average degree	110
Assortativity	-0.0619785
Number of triangles	670.6K
Average number of triangles	5.9K
Maximum number of triangles	6.1K
Average clustering coefficient	0.986349
Fraction of closed triangles	0.986032
Maximum k-core	106
Lower bound of Maximum Clique	106


Category	Temporal Networks
Collection	Temporal Reachability Networks
Tags	Dynamic network, temporal reachability, temporal graph representation, human contacts, proximity network
Source	http://www.ryanrossi.com/papers/maxclique_tscc.pdf
Short	Human contact temporal reachability graph
Vertex type	Person
Edge type	Temporal path via human contacts
Format	Undirected
Edge weights	Unweighted
Description	In networks where edges represent a contact, a phone-call, an email, or physical proximity between two entities at a specific point in time, one gets an evolving network structure. 
		One useful way to investigate temporal networks is to transform the temporal graph (sequence of timestamped edges) into a (static) temporal reachability graph representing the possible flow of information/influence, 
		etc. The temporal reachability graph is formed by placing an edge in the temporal reachability graph if there exists a "strong" temporal path between two vertices (in both directions: from u to v, and from v to u). 
		Hence, a temporal path represents a sequence of contacts that obeys time and therefore an edge in the temporal reachability graph represents the fact that a user could have transmitted a piece of information 
		or disease, etc) to that user (and vice-versa). This temporal graph representation is extremely useful for analyzing such networks and for planning applications. For instance, a temporal strong component is 
		a set of vertices where all pairwise temporal paths exist.

[LOCATION]: http://networkrepository.com/scc-infect-hyper.php