Nodes	69
Edges	911
Density	0.388321
Maximum degree	81
Minimum degree	5
Average degree	26
Assortativity	-0.272358
Number of triangles	14.6K
Average number of triangles	211
Maximum number of triangles	882
Average clustering coefficient	0.584242
Fraction of closed triangles	0.475742
Maximum k-core	21
Lower bound of Maximum Clique	9

Category Sparse networks
Collection Ecology networks
Tags Ecology, Ecology networks
Edge weights Unweighted

[LOCATION]: http://networkrepository.com/eco-everglades.php

