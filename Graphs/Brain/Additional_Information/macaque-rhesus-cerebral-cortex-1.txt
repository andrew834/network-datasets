Nodes	91
Edges	2K
Density	0.485714
Maximum degree	248
Minimum degree	6
Average degree	43
Assortativity	-0.343558
Number of triangles	86K
Average number of triangles	945
Maximum number of triangles	3.8K
Average clustering coefficient	1.30491
Fraction of closed triangles	0.556221
Maximum k-core	35
Lower bound of Maximum Clique	21

Category	Sparse networks
Collection	Brain networks
Tags Brain, brain networks, Magnetic Resonance
Source	https://neurodata.io/data/
Short	Brain networks
Edge type	Fiber tracts
Edge weights	Unweighted
Description	Edges represent fiber tracts that connect one vertex to another.

[LOCATION]: http://networkrepository.com/bn-macaque-rhesus-cerebral-cortex-1.php