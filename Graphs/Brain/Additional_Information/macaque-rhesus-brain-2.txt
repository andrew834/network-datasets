Nodes	91
Edges	628
Density	0.153358
Maximum degree	96
Minimum degree	1
Average degree	13
Assortativity	-0.70906
Number of triangles	10K
Average number of triangles	109
Maximum number of triangles	792
Average clustering coefficient	1.63385
Fraction of closed triangles	0.368904
Maximum k-core	16
Lower bound of Maximum Clique	12

Category	Sparse networks
Collection	Brain networks
Tags Brain, brain networks, Magnetic Resonance
Source	https://neurodata.io/data/
Short	Brain networks
Edge type	Fiber tracts
Edge weights	Unweighted
Description	Edges represent fiber tracts that connect one vertex to another.

[LOCATION]: http://networkrepository.com/bn-macaque-rhesus-brain-2.php