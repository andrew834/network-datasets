Nodes	1.1K
Edges	577.4K
Density	0.998271
Maximum degree	7.9K
Minimum degree	1
Average degree	1.1K
Assortativity	0.0179917
Number of triangles	777.2M
Average number of triangles	722.3K
Maximum number of triangles	8.2M
Average clustering coefficient	1.55391
Fraction of closed triangles	0.608175
Maximum k-core	1K
Lower bound of Maximum Clique	42

Category	Sparse networks
Collection	Brain networks
Tags Brain, brain networks, Magnetic Resonance
Source	https://neurodata.io/data/
Short	Brain networks
Edge type	Fiber tracts
Edge weights	Unweighted
Description	Edges represent fiber tracts that connect one vertex to another.

[LOCATOION]: http://networkrepository.com/bn-mouse-retina-1.php