Nodes 9506
Edges 193000
Maximum degree 51
Average degree 40
Assortativity 0.0386191
Number of triangles 5445150
Average number of triangles 572
Maximum number of triangles 803
Average clustering coefficient 0.711654
Fraction of closed triangles 0.692853
Maximum k-core 24
Lower bound of Maximum Clique 22
