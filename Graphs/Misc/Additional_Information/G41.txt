Nodes 2000
Edges 11785
Maximum degree 279
Average degree 11
Assortativity -0.0489534
Number of triangles 38373
Average number of triangles 19
Maximum number of triangles 866
Average clustering coefficient 0.335497
Fraction of closed triangles 0.118894
Maximum k-core 7
Lower bound of Maximum Clique 7
