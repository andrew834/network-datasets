Nodes 738
Edges 38069
Maximum degree 384
Average degree 103
Assortativity -0.137946
Number of triangles 1560642
Average number of triangles 2114
Maximum number of triangles 16388
Average clustering coefficient 0.249983
Fraction of closed triangles 0.251941
Maximum k-core 88
Lower bound of Maximum Clique 13
