Nodes 5563
Edges 140389
Maximum degree 179
Average degree 50
Assortativity 0.260562
Number of triangles 4177242
Average number of triangles 750
Maximum number of triangles 2471
Average clustering coefficient 0.607273
Fraction of closed triangles 0.583808
Maximum k-core 30
Lower bound of Maximum Clique 24
