Nodes 492
Edges 1332
Maximum degree 10
Average degree 5
Assortativity 0.194016
Number of triangles 2928
Average number of triangles 5
Maximum number of triangles 12
Average clustering coefficient 0.526839
Fraction of closed triangles 0.42983
Maximum k-core 5
Lower bound of Maximum Clique 4
