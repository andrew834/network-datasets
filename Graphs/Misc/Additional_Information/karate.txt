Nodes 34
Edges 78
Maximum degree 17
Average degree 4
Assortativity -0.475613
Number of triangles 135
Average number of triangles 3
Maximum number of triangles 18
Average clustering coefficient 0.570638
Fraction of closed triangles 0.255682
Maximum k-core 5
Lower bound of Maximum Clique 5
