Nodes 33
Edges 135
Maximum degree 19
Average degree 8
Assortativity 0.0833646
Number of triangles 756
Average number of triangles 22
Maximum number of triangles 75
Average clustering coefficient 0.507775
Fraction of closed triangles 0.574468
Maximum k-core 9
Lower bound of Maximum Clique 8
