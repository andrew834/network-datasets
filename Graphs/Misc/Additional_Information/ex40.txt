Nodes 7740
Edges 450272
Maximum degree 176
Average degree 116
Assortativity 0.179366
Number of triangles 27681624
Average number of triangles 3576
Maximum number of triangles 6632
Average clustering coefficient 0.573739
Fraction of closed triangles 0.44461
Maximum k-core 75
Lower bound of Maximum Clique 21
