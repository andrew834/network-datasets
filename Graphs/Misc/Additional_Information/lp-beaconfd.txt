Nodes 295
Edges 3374
Maximum degree 155
Average degree 22
Assortativity -0.496182
Number of triangles 3729
Average number of triangles 12
Maximum number of triangles 135
Average clustering coefficient 0.0458882
Fraction of closed triangles 0.0220669
Maximum k-core 21
Lower bound of Maximum Clique 3
