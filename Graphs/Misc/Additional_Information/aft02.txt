Nodes 8184
Edges 119578
Maximum degree 162
Average degree 29
Assortativity 0.415935
Number of triangles 1906392
Average number of triangles 232
Maximum number of triangles 6120
Average clustering coefficient 0.548366
Fraction of closed triangles 0.498013
Maximum k-core 49
Lower bound of Maximum Clique 26
