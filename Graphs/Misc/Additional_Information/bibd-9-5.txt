Nodes 126
Edges 1245
Maximum degree 45
Average degree 19
Assortativity -0.566441
Number of triangles 9063
Average number of triangles 71
Maximum number of triangles 372
Average clustering coefficient 0.39003
Fraction of closed triangles 0.236108
Maximum k-core 14
Lower bound of Maximum Clique 8
