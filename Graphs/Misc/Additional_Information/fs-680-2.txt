Nodes 680
Edges 1966
Maximum degree 19
Average degree 5
Assortativity -0.260363
Number of triangles 4371
Average number of triangles 6
Maximum number of triangles 23
Average clustering coefficient 0.709685
Fraction of closed triangles 0.207284
Maximum k-core 8
Lower bound of Maximum Clique 5
