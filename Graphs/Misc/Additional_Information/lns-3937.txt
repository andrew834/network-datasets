Nodes 3937
Edges 22204
Maximum degree 20
Average degree 11
Assortativity 0.0710641
Number of triangles 65736
Average number of triangles 16
Maximum number of triangles 40
Average clustering coefficient 0.254666
Fraction of closed triangles 0.228674
Maximum k-core 9
Lower bound of Maximum Clique 5
