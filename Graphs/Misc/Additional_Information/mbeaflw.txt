Nodes 492
Edges 49516
Maximum degree 679
Average degree 201
Assortativity -0.265887
Number of triangles 8971545
Average number of triangles 18234
Maximum number of triangles 77704
Average clustering coefficient 0.910759
Fraction of closed triangles 0.589056
Maximum k-core 142
Lower bound of Maximum Clique 80
