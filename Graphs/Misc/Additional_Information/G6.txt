Nodes 800
Edges 19176
Maximum degree 67
Average degree 47
Assortativity -0.00271391
Number of triangles 54279
Average number of triangles 67
Maximum number of triangles 153
Average clustering coefficient 0.0591687
Fraction of closed triangles 0.0592513
Maximum k-core 38
Lower bound of Maximum Clique 4
