Nodes 1000
Edges 2996
Maximum degree 8
Average degree 5
Assortativity -0.495772
Number of triangles 7984
Average number of triangles 7
Maximum number of triangles 10
Average clustering coefficient 0.678857
Fraction of closed triangles 0.47081
Maximum k-core 4
Lower bound of Maximum Clique 3
