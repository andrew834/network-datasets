Nodes 430
Edges 1178
Maximum degree 24
Average degree 5
Assortativity -0.0580971
Number of triangles 816
Average number of triangles 1
Maximum number of triangles 8
Average clustering coefficient 0.131592
Fraction of closed triangles 0.118812
Maximum k-core 5
Lower bound of Maximum Clique 3
