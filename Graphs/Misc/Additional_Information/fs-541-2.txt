Nodes 541
Edges 3744
Maximum degree 540
Average degree 13
Assortativity -0.080877
Number of triangles 19998
Average number of triangles 36
Maximum number of triangles 3204
Average clustering coefficient 0.437904
Fraction of closed triangles 0.105501
Maximum k-core 10
Lower bound of Maximum Clique 8
