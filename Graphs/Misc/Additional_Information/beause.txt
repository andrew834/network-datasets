Nodes 507
Edges 44151
Maximum degree 766
Average degree 174
Assortativity -0.315381
Number of triangles 6706349
Average number of triangles 13227
Maximum number of triangles 67189
Average clustering coefficient 0.902627
Fraction of closed triangles 0.527149
Maximum k-core 119
Lower bound of Maximum Clique 66
