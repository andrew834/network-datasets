Nodes 114
Edges 521
Maximum degree 31
Average degree 9
Assortativity -0.134746
Number of triangles 440
Average number of triangles 3
Maximum number of triangles 34
Average clustering coefficient 0.119941
Fraction of closed triangles 0.0618847
Maximum k-core 9
Lower bound of Maximum Clique 4
