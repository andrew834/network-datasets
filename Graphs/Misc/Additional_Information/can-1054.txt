Nodes 1054
Edges 5571
Maximum degree 34
Average degree 10
Assortativity -0.107928
Number of triangles 29877
Average number of triangles 28
Maximum number of triangles 110
Average clustering coefficient 0.624757
Fraction of closed triangles 0.428209
Maximum k-core 9
Lower bound of Maximum Clique 7
