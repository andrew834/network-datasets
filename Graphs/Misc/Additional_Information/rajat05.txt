Nodes 301
Edges 1086
Maximum degree 50
Average degree 7
Assortativity -0.0209388
Number of triangles 2650
Average number of triangles 8
Maximum number of triangles 14
Average clustering coefficient 0.506534
Fraction of closed triangles 0.296023
Maximum k-core 7
Lower bound of Maximum Clique 5
