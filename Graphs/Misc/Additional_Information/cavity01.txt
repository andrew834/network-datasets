Nodes 317
Edges 7084
Maximum degree 122
Average degree 44
Assortativity -0.00333458
Number of triangles 243644
Average number of triangles 768
Maximum number of triangles 2832
Average clustering coefficient 0.946079
Fraction of closed triangles 0.55371
Maximum k-core 37
Lower bound of Maximum Clique 19
