Nodes 420
Edges 3720
Maximum degree 27
Average degree 17
Assortativity 0.296221
Number of triangles 34764
Average number of triangles 82
Maximum number of triangles 156
Average clustering coefficient 0.560973
Fraction of closed triangles 0.489937
Maximum k-core 15
Lower bound of Maximum Clique 9
