Nodes 9506
Edges 265148
Maximum degree 65
Average degree 55
Assortativity 0.116548
Number of triangles 10399173
Average number of triangles 1093
Maximum number of triangles 1367
Average clustering coefficient 0.714168
Fraction of closed triangles 0.707443
Maximum k-core 33
Lower bound of Maximum Clique 26
