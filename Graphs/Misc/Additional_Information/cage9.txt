Nodes 3534
Edges 38060
Maximum degree 44
Average degree 21
Assortativity 0.728907
Number of triangles 154056
Average number of triangles 43
Maximum number of triangles 180
Average clustering coefficient 0.19519
Fraction of closed triangles 0.174821
Maximum k-core 23
Lower bound of Maximum Clique 6
