Nodes 55
Edges 1314
Maximum degree 82
Average degree 47
Assortativity -0.144569
Number of triangles 56629
Average number of triangles 1029
Maximum number of triangles 2014
Average clustering coefficient 0.94637
Fraction of closed triangles 0.806463
Maximum k-core 35
Lower bound of Maximum Clique 23
