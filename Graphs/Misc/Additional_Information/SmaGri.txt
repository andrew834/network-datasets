Nodes 1059
Edges 4918
Maximum degree 232
Average degree 9
Assortativity -0.192699
Number of triangles 17126
Average number of triangles 16
Maximum number of triangles 667
Average clustering coefficient 0.297557
Fraction of closed triangles 0.0943176
Maximum k-core 11
Lower bound of Maximum Clique 8
