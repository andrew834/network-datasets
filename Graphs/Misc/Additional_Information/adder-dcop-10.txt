Nodes 1813
Edges 9431
Maximum degree 2640
Average degree 10
Assortativity -0.18166
Number of triangles 47729
Average number of triangles 26
Maximum number of triangles 7954
Average clustering coefficient 0.654444
Fraction of closed triangles 0.0127724
Maximum k-core 9
Lower bound of Maximum Clique 5
