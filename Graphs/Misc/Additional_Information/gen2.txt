Nodes 3264
Edges 81766
Maximum degree 2240
Average degree 50
Assortativity -0.142999
Number of triangles 1506430
Average number of triangles 461
Maximum number of triangles 3095
Average clustering coefficient 0.10467
Fraction of closed triangles 0.107847
Maximum k-core 92
Lower bound of Maximum Clique 10
