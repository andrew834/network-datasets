Nodes 2283
Edges 46454
Maximum degree 90
Average degree 40
Assortativity -0.136082
Number of triangles 1021260
Average number of triangles 447
Maximum number of triangles 1416
Average clustering coefficient 0.603439
Fraction of closed triangles 0.471053
Maximum k-core 33
Lower bound of Maximum Clique 17
