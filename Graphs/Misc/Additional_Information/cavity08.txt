Nodes 1182
Edges 31864
Maximum degree 122
Average degree 53
Assortativity -0.0396587
Number of triangles 1215704
Average number of triangles 1028
Maximum number of triangles 2832
Average clustering coefficient 0.839399
Fraction of closed triangles 0.542028
Maximum k-core 37
Lower bound of Maximum Clique 19
