Nodes 6001
Edges 1131750
Maximum degree 1502
Average degree 377
Assortativity 0.665335
Number of triangles 1687499250
Average number of triangles 281203
Maximum number of triangles 1124250
Average clustering coefficient 0.24946
Fraction of closed triangles 0.997337
Maximum k-core 1501
Lower bound of Maximum Clique 1501
