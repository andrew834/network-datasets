Nodes 1220
Edges 4724
Maximum degree 70
Average degree 7
Assortativity -0.238813
Number of triangles 6784
Average number of triangles 5
Maximum number of triangles 68
Average clustering coefficient 0.243665
Fraction of closed triangles 0.0944689
Maximum k-core 6
Lower bound of Maximum Clique 3
