Nodes 24
Edges 68
Maximum degree 8
Average degree 5
Assortativity 0.0779661
Number of triangles 180
Average number of triangles 7
Maximum number of triangles 12
Average clustering coefficient 0.609524
Fraction of closed triangles 0.505618
Maximum k-core 5
Lower bound of Maximum Clique 5
