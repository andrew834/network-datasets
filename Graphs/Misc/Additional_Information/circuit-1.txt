Nodes 2624
Edges 33233
Maximum degree 5140
Average degree 25
Assortativity -0.219067
Number of triangles 473444
Average number of triangles 180
Maximum number of triangles 55688
Average clustering coefficient 0.827455
Fraction of closed triangles 0.0165014
Maximum k-core 73
Lower bound of Maximum Clique 14
