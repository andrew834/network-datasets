Nodes 2000
Edges 3584
Maximum degree 110
Average degree 3
Assortativity 0.260822
Number of triangles 68544
Average number of triangles 34
Maximum number of triangles 2516
Average clustering coefficient 0.0717771
Fraction of closed triangles 0.550891
Maximum k-core 35
Lower bound of Maximum Clique 19
