Nodes 800
Edges 19176
Maximum degree 70
Average degree 47
Assortativity -0.0120831
Number of triangles 54786
Average number of triangles 68
Maximum number of triangles 145
Average clustering coefficient 0.0598571
Fraction of closed triangles 0.0596928
Maximum k-core 38
Lower bound of Maximum Clique 4
