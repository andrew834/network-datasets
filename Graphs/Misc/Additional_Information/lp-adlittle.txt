Nodes 138
Edges 422
Maximum degree 29
Average degree 6
Assortativity -0.222308
Number of triangles 160
Average number of triangles 1
Maximum number of triangles 18
Average clustering coefficient 0.0436935
Fraction of closed triangles 0.0410256
Maximum k-core 6
Lower bound of Maximum Clique 3
