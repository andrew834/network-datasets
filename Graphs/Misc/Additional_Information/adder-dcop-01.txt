Nodes 1813
Edges 9355
Maximum degree 2640
Average degree 10
Assortativity -0.179006
Number of triangles 45560
Average number of triangles 25
Maximum number of triangles 7912
Average clustering coefficient 0.63843
Fraction of closed triangles 0.0123402
Maximum k-core 9
Lower bound of Maximum Clique 5
