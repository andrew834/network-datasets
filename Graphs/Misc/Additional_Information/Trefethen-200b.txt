Nodes 199
Edges 1337
Maximum degree 15
Average degree 13
Assortativity 0.450125
Number of triangles 3417
Average number of triangles 17
Maximum number of triangles 20
Average clustering coefficient 0.204844
Fraction of closed triangles 0.203623
Maximum k-core 9
Lower bound of Maximum Clique 3
