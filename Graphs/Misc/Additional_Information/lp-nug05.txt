Nodes 225
Edges 1045
Maximum degree 15
Average degree 9
Assortativity -0.00884868
Number of triangles 294
Average number of triangles 1
Maximum number of triangles 8
Average clustering coefficient 0.0321202
Fraction of closed triangles 0.0316537
Maximum k-core 8
Lower bound of Maximum Clique 4
