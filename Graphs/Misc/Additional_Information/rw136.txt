Nodes 136
Edges 479
Maximum degree 8
Average degree 7
Assortativity 0.437647
Number of triangles 1079
Average number of triangles 7
Maximum number of triangles 11
Average clustering coefficient 0.374895
Fraction of closed triangles 0.353539
Maximum k-core 5
Lower bound of Maximum Clique 4
