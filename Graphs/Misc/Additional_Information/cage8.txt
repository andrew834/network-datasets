Nodes 1015
Edges 9988
Maximum degree 34
Average degree 19
Assortativity 0.760892
Number of triangles 37128
Average number of triangles 36
Maximum number of triangles 100
Average clustering coefficient 0.195013
Fraction of closed triangles 0.179435
Maximum k-core 21
Lower bound of Maximum Clique 5
