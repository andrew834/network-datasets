Nodes 253
Edges 1176
Maximum degree 45
Average degree 9
Assortativity -0.439145
Number of triangles 519
Average number of triangles 2
Maximum number of triangles 79
Average clustering coefficient 0.0333579
Fraction of closed triangles 0.0290317
Maximum k-core 9
Lower bound of Maximum Clique 4
