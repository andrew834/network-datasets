Nodes 84
Edges 540
Maximum degree 51
Average degree 12
Assortativity -0.571712
Number of triangles 3649
Average number of triangles 43
Maximum number of triangles 249
Average clustering coefficient 0.474314
Fraction of closed triangles 0.284878
Maximum k-core 13
Lower bound of Maximum Clique 7
