Nodes 1922
Edges 14207
Maximum degree 32
Average degree 14
Assortativity 0.349001
Number of triangles 119796
Average number of triangles 62
Maximum number of triangles 213
Average clustering coefficient 0.567751
Fraction of closed triangles 0.527787
Maximum k-core 16
Lower bound of Maximum Clique 9
