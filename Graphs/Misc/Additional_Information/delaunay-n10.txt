Nodes 1024
Edges 3056
Maximum degree 12
Average degree 5
Assortativity -0.115349
Number of triangles 6141
Average number of triangles 5
Maximum number of triangles 12
Average clustering coefficient 0.435625
Fraction of closed triangles 0.381761
Maximum k-core 5
Lower bound of Maximum Clique 4
