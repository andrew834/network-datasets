Nodes 37
Edges 196
Maximum degree 18
Average degree 10
Assortativity 0.190212
Number of triangles 600
Average number of triangles 16
Maximum number of triangles 40
Average clustering coefficient 0.342643
Fraction of closed triangles 0.280374
Maximum k-core 9
Lower bound of Maximum Clique 4
