Nodes 2604
Edges 25408
Maximum degree 217
Average degree 19
Assortativity -0.127441
Number of triangles 152698
Average number of triangles 58
Maximum number of triangles 3970
Average clustering coefficient 0.045418
Fraction of closed triangles 0.0938386
Maximum k-core 35
Lower bound of Maximum Clique 9
