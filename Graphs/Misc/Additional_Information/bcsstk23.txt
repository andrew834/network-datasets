Nodes 3134
Edges 21022
Maximum degree 30
Average degree 13
Assortativity 0.679147
Number of triangles 89211
Average number of triangles 28
Maximum number of triangles 136
Average clustering coefficient 0.274446
Fraction of closed triangles 0.299254
Maximum k-core 15
Lower bound of Maximum Clique 12
