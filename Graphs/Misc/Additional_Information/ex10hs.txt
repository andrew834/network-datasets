Nodes 2548
Edges 27380
Maximum degree 49
Average degree 21
Assortativity 0.304451
Number of triangles 407223
Average number of triangles 159
Maximum number of triangles 504
Average clustering coefficient 0.696026
Fraction of closed triangles 0.562661
Maximum k-core 18
Lower bound of Maximum Clique 18
