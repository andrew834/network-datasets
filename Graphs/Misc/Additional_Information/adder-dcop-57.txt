Nodes 1813
Edges 9445
Maximum degree 2640
Average degree 10
Assortativity -0.181332
Number of triangles 47830
Average number of triangles 26
Maximum number of triangles 7976
Average clustering coefficient 0.65352
Fraction of closed triangles 0.0127988
Maximum k-core 9
Lower bound of Maximum Clique 5
