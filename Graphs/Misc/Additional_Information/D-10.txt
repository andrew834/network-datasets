Nodes 815
Edges 7591
Maximum degree 51
Average degree 18
Assortativity -0.059273
Number of triangles 6054
Average number of triangles 7
Maximum number of triangles 54
Average clustering coefficient 0.0346134
Fraction of closed triangles 0.0340616
Maximum k-core 13
Lower bound of Maximum Clique 4
