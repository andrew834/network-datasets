Nodes 4257
Edges 16604
Maximum degree 8
Average degree 7
Assortativity 0.383697
Number of triangles 49392
Average number of triangles 11
Maximum number of triangles 12
Average clustering coefficient 0.440223
Fraction of closed triangles 0.432777
Maximum k-core 5
Lower bound of Maximum Clique 4
