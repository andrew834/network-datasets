Nodes 363
Edges 2452
Maximum degree 41
Average degree 13
Assortativity 0.129805
Number of triangles 741
Average number of triangles 2
Maximum number of triangles 50
Average clustering coefficient 0.0171311
Fraction of closed triangles 0.0144318
Maximum k-core 17
Lower bound of Maximum Clique 3
