Nodes 5177
Edges 15004
Maximum degree 10
Average degree 5
Assortativity -0.357108
Number of triangles 29400
Average number of triangles 5
Maximum number of triangles 10
Average clustering coefficient 0.473574
Fraction of closed triangles 0.359717
Maximum k-core 4
Lower bound of Maximum Clique 3
