Nodes 1132
Edges 12377
Maximum degree 46
Average degree 21
Assortativity 0.0271792
Number of triangles 7917
Average number of triangles 6
Maximum number of triangles 36
Average clustering coefficient 0.024892
Fraction of closed triangles 0.0260314
Maximum k-core 15
Lower bound of Maximum Clique 4
