Nodes 800
Edges 4672
Maximum degree 123
Average degree 11
Assortativity -0.0593109
Number of triangles 15957
Average number of triangles 19
Maximum number of triangles 373
Average clustering coefficient 0.347534
Fraction of closed triangles 0.154232
Maximum k-core 7
Lower bound of Maximum Clique 7
