Nodes 760
Edges 5216
Maximum degree 40
Average degree 13
Assortativity -0.0311478
Number of triangles 32520
Average number of triangles 42
Maximum number of triangles 191
Average clustering coefficient 0.422016
Fraction of closed triangles 0.353663
Maximum k-core 11
Lower bound of Maximum Clique 7
