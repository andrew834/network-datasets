Nodes 3402
Edges 134443
Maximum degree 181
Average degree 79
Assortativity 0.167801
Number of triangles 5652495
Average number of triangles 1661
Maximum number of triangles 3522
Average clustering coefficient 0.546006
Fraction of closed triangles 0.489834
Maximum k-core 47
Lower bound of Maximum Clique 24
