Nodes 636
Edges 6132
Maximum degree 35
Average degree 19
Assortativity 0.0584965
Number of triangles 4776
Average number of triangles 7
Maximum number of triangles 31
Average clustering coefficient 0.0379097
Fraction of closed triangles 0.0378237
Maximum k-core 14
Lower bound of Maximum Clique 4
