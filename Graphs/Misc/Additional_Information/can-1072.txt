Nodes 1072
Edges 5686
Maximum degree 34
Average degree 10
Assortativity -0.11064
Number of triangles 30531
Average number of triangles 28
Maximum number of triangles 111
Average clustering coefficient 0.624434
Fraction of closed triangles 0.425656
Maximum k-core 9
Lower bound of Maximum Clique 7
