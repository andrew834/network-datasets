Nodes 147
Edges 1147
Maximum degree 20
Average degree 15
Assortativity 0.562969
Number of triangles 10893
Average number of triangles 74
Maximum number of triangles 109
Average clustering coefficient 0.632976
Fraction of closed triangles 0.598977
Maximum k-core 12
Lower bound of Maximum Clique 9
