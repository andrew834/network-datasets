Nodes 468
Edges 2352
Maximum degree 22
Average degree 10
Assortativity 0.416745
Number of triangles 3561
Average number of triangles 7
Maximum number of triangles 44
Average clustering coefficient 0.145411
Fraction of closed triangles 0.156852
Maximum k-core 8
Lower bound of Maximum Clique 8
