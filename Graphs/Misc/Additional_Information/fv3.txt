Nodes 9801
Edges 38612
Maximum degree 8
Average degree 7
Assortativity 0.391415
Number of triangles 115248
Average number of triangles 11
Maximum number of triangles 12
Average clustering coefficient 0.435591
Fraction of closed triangles 0.431085
Maximum k-core 5
Lower bound of Maximum Clique 4
