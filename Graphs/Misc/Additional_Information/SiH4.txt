Nodes 5041
Edges 83431
Maximum degree 204
Average degree 33
Assortativity 0.861094
Number of triangles 3279921
Average number of triangles 650
Maximum number of triangles 15903
Average clustering coefficient 0.275903
Fraction of closed triangles 0.607972
Maximum k-core 179
Lower bound of Maximum Clique 179
