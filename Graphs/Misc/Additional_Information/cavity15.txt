Nodes 312
Edges 1957
Maximum degree 86
Average degree 12
Assortativity -0.328962
Number of triangles 43778
Average number of triangles 140
Maximum number of triangles 1154
Average clustering coefficient 0.745469
Fraction of closed triangles 0.525515
Maximum k-core 27
Lower bound of Maximum Clique 14
