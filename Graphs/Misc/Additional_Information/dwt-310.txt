Nodes 310
Edges 1069
Maximum degree 10
Average degree 6
Assortativity 0.349094
Number of triangles 2976
Average number of triangles 9
Maximum number of triangles 15
Average clustering coefficient 0.478994
Fraction of closed triangles 0.448193
Maximum k-core 5
Lower bound of Maximum Clique 4
