Nodes 544
Edges 4321
Maximum degree 38
Average degree 15
Assortativity -0.0322433
Number of triangles 3760
Average number of triangles 6
Maximum number of triangles 38
Average clustering coefficient 0.0494781
Fraction of closed triangles 0.0465197
Maximum k-core 12
Lower bound of Maximum Clique 4
