Nodes 4515
Edges 46596
Maximum degree 26
Average degree 20
Assortativity 0.789179
Number of triangles 508464
Average number of triangles 112
Maximum number of triangles 181
Average clustering coefficient 0.52946
Fraction of closed triangles 0.523713
Maximum k-core 15
Lower bound of Maximum Clique 10
