Nodes 1022
Edges 5074
Maximum degree 39
Average degree 9
Assortativity 0.220435
Number of triangles 9310
Average number of triangles 9
Maximum number of triangles 93
Average clustering coefficient 0.140675
Fraction of closed triangles 0.135343
Maximum k-core 9
Lower bound of Maximum Clique 6
