Nodes 398
Edges 1256
Maximum degree 12
Average degree 6
Assortativity 0.219626
Number of triangles 930
Average number of triangles 2
Maximum number of triangles 6
Average clustering coefficient 0.14737
Fraction of closed triangles 0.117602
Maximum k-core 5
Lower bound of Maximum Clique 4
