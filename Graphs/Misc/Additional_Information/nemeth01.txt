Nodes 9506
Edges 357774
Maximum degree 95
Average degree 75
Assortativity -0.0145659
Number of triangles 19556550
Average number of triangles 2057
Maximum number of triangles 2812
Average clustering coefficient 0.740252
Fraction of closed triangles 0.721135
Maximum k-core 42
Lower bound of Maximum Clique 37
