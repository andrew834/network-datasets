Nodes 398
Edges 3280
Maximum degree 38
Average degree 16
Assortativity 0.0536582
Number of triangles 23228
Average number of triangles 58
Maximum number of triangles 194
Average clustering coefficient 0.439026
Fraction of closed triangles 0.377151
Maximum k-core 11
Lower bound of Maximum Clique 6
