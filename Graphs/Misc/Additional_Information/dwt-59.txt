Nodes 59
Edges 104
Maximum degree 5
Average degree 3
Assortativity 0.143071
Number of triangles 90
Average number of triangles 1
Maximum number of triangles 4
Average clustering coefficient 0.258757
Fraction of closed triangles 0.310345
Maximum k-core 4
Lower bound of Maximum Clique 3
