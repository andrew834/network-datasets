Nodes 62
Edges 388
Maximum degree 36
Average degree 12
Assortativity 0.101314
Number of triangles 2332
Average number of triangles 37
Maximum number of triangles 187
Average clustering coefficient 0.453076
Fraction of closed triangles 0.418822
Maximum k-core 11
Lower bound of Maximum Clique 6
