Nodes 9506
Edges 192651
Maximum degree 51
Average degree 40
Assortativity 0.0395815
Number of triangles 5401194
Average number of triangles 568
Maximum number of triangles 800
Average clustering coefficient 0.708757
Fraction of closed triangles 0.690034
Maximum k-core 24
Lower bound of Maximum Clique 21
