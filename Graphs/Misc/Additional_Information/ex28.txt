Nodes 2603
Edges 75178
Maximum degree 122
Average degree 57
Assortativity -0.0649347
Number of triangles 2926320
Average number of triangles 1124
Maximum number of triangles 2832
Average clustering coefficient 0.740864
Fraction of closed triangles 0.540997
Maximum k-core 37
Lower bound of Maximum Clique 19
