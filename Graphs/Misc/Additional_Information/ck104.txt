Nodes 104
Edges 888
Maximum degree 18
Average degree 17
Assortativity 0.426145
Number of triangles 9312
Average number of triangles 89
Maximum number of triangles 96
Average clustering coefficient 0.656777
Fraction of closed triangles 0.639912
Maximum k-core 11
Lower bound of Maximum Clique 7
