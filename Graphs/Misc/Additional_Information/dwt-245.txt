Nodes 245
Edges 608
Maximum degree 12
Average degree 4
Assortativity 0.315878
Number of triangles 1122
Average number of triangles 4
Maximum number of triangles 23
Average clustering coefficient 0.298103
Fraction of closed triangles 0.346832
Maximum k-core 6
Lower bound of Maximum Clique 5
