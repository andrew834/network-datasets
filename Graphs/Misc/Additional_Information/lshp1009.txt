Nodes 1009
Edges 2928
Maximum degree 6
Average degree 5
Assortativity 0.46471
Number of triangles 5760
Average number of triangles 5
Maximum number of triangles 6
Average clustering coefficient 0.410439
Fraction of closed triangles 0.404239
Maximum k-core 4
Lower bound of Maximum Clique 3
