Nodes 17
Edges 38
Maximum degree 10
Average degree 4
Assortativity -0.542349
Number of triangles 36
Average number of triangles 2
Maximum number of triangles 6
Average clustering coefficient 0.185808
Fraction of closed triangles 0.1875
Maximum k-core 5
Lower bound of Maximum Clique 3
