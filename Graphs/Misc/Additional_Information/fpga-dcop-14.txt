Nodes 1220
Edges 4756
Maximum degree 70
Average degree 7
Assortativity -0.23681
Number of triangles 6912
Average number of triangles 5
Maximum number of triangles 68
Average clustering coefficient 0.247195
Fraction of closed triangles 0.09575
Maximum k-core 6
Lower bound of Maximum Clique 3
