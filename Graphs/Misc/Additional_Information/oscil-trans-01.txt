Nodes 430
Edges 1218
Maximum degree 24
Average degree 5
Assortativity -0.0302005
Number of triangles 1022
Average number of triangles 2
Maximum number of triangles 16
Average clustering coefficient 0.14995
Fraction of closed triangles 0.138145
Maximum k-core 7
Lower bound of Maximum Clique 5
