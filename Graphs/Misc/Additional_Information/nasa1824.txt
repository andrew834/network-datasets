Nodes 1824
Edges 18692
Maximum degree 41
Average degree 20
Assortativity 0.401831
Number of triangles 215829
Average number of triangles 118
Maximum number of triangles 281
Average clustering coefficient 0.589707
Fraction of closed triangles 0.526888
Maximum k-core 18
Lower bound of Maximum Clique 12
