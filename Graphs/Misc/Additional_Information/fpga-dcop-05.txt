Nodes 1220
Edges 4716
Maximum degree 70
Average degree 7
Assortativity -0.239001
Number of triangles 6752
Average number of triangles 5
Maximum number of triangles 68
Average clustering coefficient 0.242163
Fraction of closed triangles 0.0941334
Maximum k-core 6
Lower bound of Maximum Clique 3
