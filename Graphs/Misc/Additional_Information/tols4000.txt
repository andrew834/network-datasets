Nodes 4000
Edges 5584
Maximum degree 110
Average degree 2
Assortativity 0.370844
Number of triangles 68544
Average number of triangles 17
Maximum number of triangles 2516
Average clustering coefficient 0.0358886
Fraction of closed triangles 0.532075
Maximum k-core 35
Lower bound of Maximum Clique 19
