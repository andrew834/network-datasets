Nodes 292
Edges 1124
Maximum degree 34
Average degree 7
Assortativity -0.205297
Number of triangles 3282
Average number of triangles 11
Maximum number of triangles 63
Average clustering coefficient 0.464159
Fraction of closed triangles 0.278466
Maximum k-core 7
Lower bound of Maximum Clique 5
