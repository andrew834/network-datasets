Nodes 472
Edges 2486
Maximum degree 75
Average degree 10
Assortativity -0.0537034
Number of triangles 1512
Average number of triangles 3
Maximum number of triangles 87
Average clustering coefficient 0.0359826
Fraction of closed triangles 0.0367704
Maximum k-core 13
Lower bound of Maximum Clique 4
