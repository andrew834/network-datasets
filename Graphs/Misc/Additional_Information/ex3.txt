Nodes 1821
Edges 25432
Maximum degree 61
Average degree 27
Assortativity 0.0162059
Number of triangles 451428
Average number of triangles 247
Maximum number of triangles 630
Average clustering coefficient 0.70222
Fraction of closed triangles 0.561135
Maximum k-core 18
Lower bound of Maximum Clique 18
