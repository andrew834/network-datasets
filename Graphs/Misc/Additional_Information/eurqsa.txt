Nodes 7245
Edges 20509
Maximum degree 37
Average degree 5
Assortativity -0.118337
Number of triangles 11529
Average number of triangles 1
Maximum number of triangles 3
Average clustering coefficient 0.212886
Fraction of closed triangles 0.0717495
Maximum k-core 5
Lower bound of Maximum Clique 3
