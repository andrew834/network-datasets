Nodes 7000
Edges 41459
Maximum degree 589
Average degree 11
Assortativity -0.0341557
Number of triangles 127638
Average number of triangles 18
Maximum number of triangles 1640
Average clustering coefficient 0.324358
Fraction of closed triangles 0.0928786
Maximum k-core 7
Lower bound of Maximum Clique 6
