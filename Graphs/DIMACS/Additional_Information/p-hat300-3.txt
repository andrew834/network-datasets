Nodes	300
Edges	33.4K
Density	0.744482
Maximum degree	267
Minimum degree	168
Average degree	222
Assortativity	-0.0204908
Number of triangles	5.7M
Average number of triangles	18.9K
Maximum number of triangles	26.7K
Average clustering coefficient	0.758296
Fraction of closed triangles	0.757799
Maximum k-core	181
Lower bound of Maximum Clique	33
