Nodes	1K
Edges	3.1K
Density	0.00583456
Maximum degree	12
Minimum degree	3
Average degree	5
Assortativity	-0.115349
Number of triangles	6.1K
Average number of triangles	5
Maximum number of triangles	12
Average clustering coefficient	0.435625
Fraction of closed triangles	0.381761
Maximum k-core	5
Lower bound of Maximum Clique	4

Tags	undirected graph
Author	M. Holtgrewe, P. Sanders, C. Schulz
Date	2011
Edge weights	Unweighted
Metadata	undirected graph
Description	DIMACS10 set

