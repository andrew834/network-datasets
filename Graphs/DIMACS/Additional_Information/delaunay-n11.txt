Nodes	1.5K
Edges	19K
Density	0.0171477
Maximum degree	467
Minimum degree	0
Average degree	25
Assortativity	-0.196103
Number of triangles	459.4K
Average number of triangles	308
Maximum number of triangles	9.5K
Average clustering coefficient	0.312823
Fraction of closed triangles	0.251111
Maximum k-core	44
Lower bound of Maximum Clique	19

Tags	undirected graph
Author	M. Holtgrewe, P. Sanders, C. Schulz
Date	2011
Edge weights	Unweighted
Metadata	undirected graph
Description	DIMACS10 set

