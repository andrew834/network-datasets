Nodes	8.2K
Edges	24.5K
Density	0.000731647
Maximum degree	12
Minimum degree	3
Average degree	5
Assortativity	-0.12776
Number of triangles	49.3K
Average number of triangles	6
Maximum number of triangles	12
Average clustering coefficient	0.4335
Fraction of closed triangles	0.379588
Maximum k-core	5
Lower bound of Maximum Clique	4

Tags	undirected graph
Author	M. Holtgrewe, P. Sanders, C. Schulz
Date	2011
Edge weights	Unweighted
Metadata	undirected graph
Description	DIMACS10 set