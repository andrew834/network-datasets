Nodes	171
Edges	9.4K
Density	0.649123
Maximum degree	124
Minimum degree	102
Average degree	110
Assortativity	-0.0709832
Number of triangles	649.8K
Average number of triangles	3.8K
Maximum number of triangles	4.8K
Average clustering coefficient	0.627437
Fraction of closed triangles	0.62698
Maximum k-core	103
Lower bound of Maximum Clique	9