Nodes	29
Edges	82
Density	0.202
Maximum degree	5
Minimum degree	0
Average degree	2.83
Assortativity	-0.06
Number of triangles	15
Average number of triangles	0.52
Maximum number of triangles	2
Average clustering coefficient	0.191
Fraction of closed triangles	0.179
Maximum k-core	2
Maximum k-truss	1
Upper bound of Chromatic number	3

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g150.php