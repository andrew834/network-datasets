Nodes	24
Edges	100
Density	0.3623
Maximum degree	7
Minimum degree	0
Average degree	4.17
Assortativity	-0.05
Number of triangles	81
Average number of triangles	3.38
Maximum number of triangles	6
Average clustering coefficient	0.564
Fraction of closed triangles	0.491
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	5

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g40.php