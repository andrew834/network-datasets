Nodes	11
Edges	46
Density	0.8364
Maximum degree	5
Minimum degree	0
Average degree	4.18
Assortativity	-0.1
Number of triangles	40
Average number of triangles	3.64
Maximum number of triangles	6
Average clustering coefficient	0.562
Fraction of closed triangles	0.563
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g127.php