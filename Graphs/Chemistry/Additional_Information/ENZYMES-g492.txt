Nodes	38
Edges	158
Density	0.2248
Maximum degree	7
Minimum degree	0
Average degree	4.16
Assortativity	-0.01
Number of triangles	120
Average number of triangles	3.16
Maximum number of triangles	7
Average clustering coefficient	0.51
Fraction of closed triangles	0.444
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	5

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g492.php