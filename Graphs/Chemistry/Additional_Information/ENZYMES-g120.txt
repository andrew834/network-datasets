Nodes	22
Edges	92
Density	0.3983
Maximum degree	7
Minimum degree	0
Average degree	4.18
Assortativity	-0.02
Number of triangles	68
Average number of triangles	3.09
Maximum number of triangles	9
Average clustering coefficient	0.465
Fraction of closed triangles	0.422
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	5

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g120.php