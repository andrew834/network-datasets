Nodes	19
Edges	84
Density	0.4912
Maximum degree	7
Minimum degree	0
Average degree	4.42
Assortativity	-0.09
Number of triangles	39
Average number of triangles	2.05
Maximum number of triangles	4
Average clustering coefficient	0.387
Fraction of closed triangles	0.262
Maximum k-core	3
Maximum k-truss	1
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g354.php