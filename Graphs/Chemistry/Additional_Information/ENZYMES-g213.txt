Nodes	25
Edges	110
Density	0.3667
Maximum degree	7
Minimum degree	0
Average degree	4.4
Assortativity	-0.04
Number of triangles	68
Average number of triangles	2.72
Maximum number of triangles	7
Average clustering coefficient	0.373
Fraction of closed triangles	0.345
Maximum k-core	3
Maximum k-truss	1
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: hhttp://networkrepository.com/ENZYMES-g213.php