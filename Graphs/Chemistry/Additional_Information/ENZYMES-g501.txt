Nodes	66
Edges	160
Density	0.0746
Maximum degree	6
Minimum degree	0
Average degree	2.42
Assortativity	0
Number of triangles	6
Average number of triangles	0.09
Maximum number of triangles	2
Average clustering coefficient	0.01
Fraction of closed triangles	0.04
Maximum k-core	3
Maximum k-truss	1
Upper bound of Chromatic number	3

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g501.php