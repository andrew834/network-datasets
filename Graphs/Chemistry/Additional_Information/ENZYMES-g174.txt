Nodes	48
Edges	196
Density	0.1738
Maximum degree	6
Minimum degree	0
Average degree	4.08
Assortativity	-0.02
Number of triangles	83
Average number of triangles	1.73
Maximum number of triangles	5
Average clustering coefficient	0.269
Fraction of closed triangles	0.261
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g174.php