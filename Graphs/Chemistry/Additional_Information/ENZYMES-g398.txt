Nodes	14
Edges	46
Density	0.5055
Maximum degree	4
Minimum degree	0
Average degree	3.29
Assortativity	-0.11
Number of triangles	4
Average number of triangles	0.29
Maximum number of triangles	1.5
Average clustering coefficient	0.101
Fraction of closed triangles	0.083
Maximum k-core	2
Maximum k-truss	1
Upper bound of Chromatic number	2

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g398.php