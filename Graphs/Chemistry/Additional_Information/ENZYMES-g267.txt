Nodes	31
Edges	118
Density	0.2538
Maximum degree	7
Minimum degree	0
Average degree	3.81
Assortativity	-0.05
Number of triangles	95
Average number of triangles	3.06
Maximum number of triangles	7
Average clustering coefficient	0.613
Fraction of closed triangles	0.549
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g267.php