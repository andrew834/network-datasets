Nodes	16
Edges	72
Density	0.6
Maximum degree	7
Minimum degree	0
Average degree	4.5
Assortativity	-0.06
Number of triangles	69
Average number of triangles	4.31
Maximum number of triangles	7
Average clustering coefficient	0.565
Fraction of closed triangles	0.523
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	5

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g138.php