Nodes	16
Edges	64
Density	0.5333
Maximum degree	7
Minimum degree	0
Average degree	4
Assortativity	-0.08
Number of triangles	63
Average number of triangles	3.94
Maximum number of triangles	8
Average clustering coefficient	0.675
Fraction of closed triangles	0.606
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g281.php