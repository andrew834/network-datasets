Nodes	39
Edges	122
Density	0.1646
Maximum degree	6
Minimum degree	0
Average degree	3.13
Assortativity	-0.05
Number of triangles	20
Average number of triangles	0.51
Maximum number of triangles	4
Average clustering coefficient	0.063
Fraction of closed triangles	0.123
Maximum k-core	3
Maximum k-truss	1
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g143.php