Nodes	55
Edges	194
Density	0.1306
Maximum degree	6
Minimum degree	0
Average degree	3.53
Assortativity	-0.03
Number of triangles	66
Average number of triangles	1.2
Maximum number of triangles	6
Average clustering coefficient	0.182
Fraction of closed triangles	0.232
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g16.php