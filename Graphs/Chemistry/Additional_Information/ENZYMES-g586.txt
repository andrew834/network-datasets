Nodes	24
Edges	102
Density	0.3696
Maximum degree	6
Minimum degree	0
Average degree	4.25
Assortativity	-0.05
Number of triangles	87
Average number of triangles	3.63
Maximum number of triangles	7
Average clustering coefficient	0.547
Fraction of closed triangles	0.518
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	5

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g586.php