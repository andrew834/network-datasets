Nodes	39
Edges	162
Density	0.2186
Maximum degree	6
Minimum degree	0
Average degree	4.15
Assortativity	-0.02
Number of triangles	126
Average number of triangles	3.23
Maximum number of triangles	7
Average clustering coefficient	0.523
Fraction of closed triangles	0.477
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g23.php