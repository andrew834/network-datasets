Nodes	18
Edges	66
Density	0.4314
Maximum degree	7
Minimum degree	0
Average degree	3.67
Assortativity	-0.1
Number of triangles	23
Average number of triangles	1.28
Maximum number of triangles	5.5
Average clustering coefficient	0.198
Fraction of closed triangles	0.237
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g155.php