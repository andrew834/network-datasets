Nodes	22
Edges	96
Density	0.4156
Maximum degree	7
Minimum degree	0
Average degree	4.36
Assortativity	-0.02
Number of triangles	78
Average number of triangles	3.55
Maximum number of triangles	8
Average clustering coefficient	0.502
Fraction of closed triangles	0.459
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	4


Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g160.php