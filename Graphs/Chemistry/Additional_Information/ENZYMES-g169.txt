Nodes	44
Edges	188
Density	0.1987
Maximum degree	6
Minimum degree	0
Average degree	4.27
Assortativity	-0
Number of triangles	74
Average number of triangles	1.68
Maximum number of triangles	7
Average clustering coefficient	0.274
Fraction of closed triangles	0.226
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g169.php