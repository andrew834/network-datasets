Nodes	17
Edges	58
Density	0.4265
Maximum degree	5
Minimum degree	0
Average degree	3.41
Assortativity	-0.08
Number of triangles	39
Average number of triangles	2.29
Maximum number of triangles	4
Average clustering coefficient	0.597
Fraction of closed triangles	0.582
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g78.php