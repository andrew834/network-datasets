Nodes	54
Edges	210
Density	0.1468
Maximum degree	8
Minimum degree	0
Average degree	3.89
Assortativity	-0.02
Number of triangles	96
Average number of triangles	1.78
Maximum number of triangles	4
Average clustering coefficient	0.303
Fraction of closed triangles	0.294
Maximum k-core	3
Maximum k-truss	1
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g224.php