Nodes	48
Edges	160
Density	0.1418
Maximum degree	7
Minimum degree	0
Average degree	3.33
Assortativity	-0.04
Number of triangles	7
Average number of triangles	0.15
Maximum number of triangles	1.5
Average clustering coefficient	0.034
Fraction of closed triangles	0.035
Maximum k-core	2
Maximum k-truss	1

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g563.php