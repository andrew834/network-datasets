Nodes	28
Edges	130
Density	0.3439
Maximum degree	7
Minimum degree	0
Average degree	4.64
Assortativity	-0.04
Number of triangles	73
Average number of triangles	2.61
Maximum number of triangles	6
Average clustering coefficient	0.296
Fraction of closed triangles	0.284
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	5

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g460.php