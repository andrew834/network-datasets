Nodes	52
Edges	174
Density	0.1312
Maximum degree	5
Minimum degree	0
Average degree	3.35
Assortativity	-0.03
Number of triangles	35
Average number of triangles	0.67
Maximum number of triangles	4
Average clustering coefficient	0.108
Fraction of closed triangles	0.157
Maximum k-core	3
Maximum k-truss	1
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g283.php