Nodes	18
Edges	75
Density	0.490196
Maximum degree	16
Minimum degree	2
Average degree	8
Assortativity	-0.209384
Number of triangles	418
Average number of triangles	23
Maximum number of triangles	55
Average clustering coefficient	0.744682
Fraction of closed triangles	0.618343
Maximum k-core	8
Lower bound of Maximum Clique	6


Tags	Southern Women Club
Description	This dataset was collected by Davis and colleague in the 1930s. It contains the observed attendance at 14 social events by 18 Southern women.

[LOCATION]: http://networkrepository.com/ia-southernwomen.php