Nodes	6.8K
Edges	7.7K
Density	0.000331351
Maximum degree	261
Minimum degree	1
Average degree	2
Assortativity	-0.675344
Number of triangles	1.2K
Average number of triangles	0
Maximum number of triangles	52
Average clustering coefficient	0.0177709
Fraction of closed triangles	0.00240715
Maximum k-core	6
Lower bound of Maximum Clique	5

Category	Sparse networks, temporal networks
Collection	Interaction networks
Tags	human contacts, mobile, call network interaction, networktemporal network
Source	http://realitycommons.media.mit.edu/realitymining.html
Short	user-calls-user
Vertex type	Person
Edge type	Call
Format	Undirected
Edge weights	Multiple unweighted edges
Metadata	Time (edges have timestamps)
Description	Reality mining network data consists of human mobile phone call events between a small set of core users at the Massachusetts Institute of Technology (MIT) 
		whom actually were assigned mobile phones for which all calls were collected. The data also contains calls from users outside this small set of users to other
	        phones of individuals that were not actively monitored and thus these nodes generally have fewer edges than nodes within the small set of users at MIT that 
		participated in the experiment and were assigned phones. The data was collected collected by the Reality Mining experiment performed in 2004 as part of the Reality Commons project. 
		The data was collected over 9 months using 100 mobile phones. A node represents a person; an edge indicates a phone call or voicemail between two users. 

[LOCATION]:  http://networkrepository.com/ia-reality.php

