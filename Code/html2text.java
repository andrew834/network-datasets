import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.tidy.DOMElementImpl;
import org.w3c.tidy.Tidy;

@SuppressWarnings("resource")
public class html2text {

	static String[] imenaAtributov = { "Nodes", "Edges", "Maximum degree", "Average degree", "Assortativity",
			"Number of triangles", "Average number of triangles", "Maximum number of triangles",
			"Average clustering coefficient", "Fraction of closed triangles", "Maximum k-core",
			"Lower bound of Maximum Clique" };

	public static void main(String[] args) throws MalformedURLException, IOException, URISyntaxException {

		final Tidy tidy = new Tidy();
		tidy.setMakeClean(true);
		tidy.setXHTML(true);
		tidy.setSmartIndent(true);
		String s = new Scanner(new URL("http://networkrepository.com/misc.php").openStream(), "UTF-8")
				.useDelimiter("\\A").next();

		// Converting the page into a Document instance
		final Document doc = tidy.parseDOM(new ByteArrayInputStream(s.getBytes()), null);
		doc.getDocumentElement().normalize();

		DOMElementImpl items = (DOMElementImpl) doc.getChildNodes().item(5);
		DOMElementImpl body = (DOMElementImpl) items.getChildNodes().item(3);
		DOMElementImpl div = (DOMElementImpl) body.getChildNodes().item(7);
		DOMElementImpl div2 = (DOMElementImpl) div.getChildNodes().item(1);
		DOMElementImpl div3 = (DOMElementImpl) div2.getChildNodes().item(0);
		DOMElementImpl table = (DOMElementImpl) div3.getChildNodes().item(2);
		System.out.println(table.getNodeName());

		int count = 0;
		for (int i = 0; i < table.getChildNodes().getLength(); i++) {
			DOMElementImpl element = (DOMElementImpl) table.getChildNodes().item(i);

			if (element.getChildNodes().getLength() >= 2) {

				Node el = element.getChildNodes().item(1);
				String[] val = el.getAttributes().item(0).getNodeValue().split(" ");
				int num = Integer.parseInt(val[1].replace("}", ""));

				if (num <= 10000 && num > 0) {

					count++;
					String original = element.getAttribute("data-url").replaceAll(".php", "");
					String filename = element.getAttribute("data-url").replace("php", "zip").replace("-", "_");

					System.out.println(original);
					PrintWriter writer = new PrintWriter(original + ".txt", "UTF-8");

					for (int j = 1; j < imenaAtributov.length + 1; j++) {
						Node n = element.getChildNodes().item(j);
						String[] ime = n.getAttributes().item(0).getNodeValue().split(" ");
						// System.out.print(imenaAtributov[j-1]+": "+ime[1].substring(0,
						// ime[1].length()-1)+" | ");
						writer.println(imenaAtributov[j - 1] + " " + ime[1].substring(0, ime[1].length() - 1));
					}

					writer.close();

					System.out.println();

					String url1 = "http://nrvis.com/download/data/misc/" + filename;
					System.out.println(url1);

					URL url = new URL(url1);
					HttpURLConnection connection = (HttpURLConnection) url.openConnection();
					connection.setRequestMethod("GET");
					InputStream in = connection.getInputStream();
					FileOutputStream out = new FileOutputStream("C:\\DOWNLOAD\\" + filename);
					copy(in, out, 1024);
					out.close();
				}
			}
		}
		System.out.println("Count: " + count);
	}

	public static void copy(InputStream input, OutputStream output, int bufferSize) throws IOException {
		byte[] buf = new byte[bufferSize];
		int n = input.read(buf);
		while (n >= 0) {
			output.write(buf, 0, n);
			n = input.read(buf);
		}
		output.flush();
	}
}