import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;

public class Pattern_generation {
	
	public static void writeOut(int m, TreeMap<Integer, TreeSet<Integer>> t) throws FileNotFoundException, UnsupportedEncodingException {
		File out = new File("Pattern_"+m);
		PrintWriter writer = new PrintWriter("Pattern_"+m+".edges", "UTF-8");
		
		for (int i = 0; i < t.size(); i++) {
			TreeSet<Integer> temp = t.get(i);
			for (Integer integer : temp) {
				writer.println(i+" "+integer);
			}	
		}
		writer.flush();
		writer.close();
	}
	
	public static TreeMap<Integer, TreeSet<Integer>> returnCopy(TreeMap<Integer, TreeSet<Integer>> t){
		TreeMap<Integer, TreeSet<Integer>> ret = new TreeMap<>();
		
		for (int i = 0; i < t.size(); i++) {
			TreeSet<Integer> ts = new TreeSet<>();
			TreeSet<Integer> t_orig = t.get(i);
			for (Integer integer : t_orig) {
				ts.add(integer);
			}
			ret.put(i, ts);
		}
		
		return ret;
	}
	
	public static boolean is_connected(TreeMap<Integer, TreeSet<Integer>> temp_map) throws InterruptedException {

		TreeMap<Integer, TreeSet<Integer>> temp = returnCopy(temp_map);
		Queue<Integer> q = new LinkedList<Integer>();
		while(!temp.get(0).isEmpty()) {
			int t = temp.get(0).first();
			q.add(t);
			temp.get(0).remove(t);
		}
	
		temp.remove(0);
		
		while(!q.isEmpty()) {
			int a = q.remove();
			if(temp.get(a)!=null) {
				while(temp.get(a).size()>0) {
					q.add(temp.get(a).first());
					temp.get(a).remove(temp.get(a).first());
				}
				temp.remove(a);
			}
		}
		return temp.isEmpty();
	}
	
	public static TreeMap<Integer, TreeSet<Integer>> make_connections(double p, int n) {
		TreeMap<Integer, TreeSet<Integer>> temp_map = create_nodes(n);
		//int edges = 0;
		for (int i = 0; i < n-1; i++) {
			for (int j = i+1; j < n; j++) {
				if(p>=Math.random()) {
					temp_map.get(i).add(j);
					temp_map.get(j).add(i);
					//edges+=2;
				}
			}
			if(temp_map.get(i).size()==0) {
				i--;
			}
		}
		
		if(temp_map.get(temp_map.size()-1).size()==0) {
			int connect = (int)(Math.random()*n);
			temp_map.get(n-1).add(connect);
			temp_map.get(connect).add(n-1);
			//edges++;
		}
		//System.out.println(edges);
		return temp_map;
	}
	
	public static long c = 0;
	
	public static TreeMap<Integer, TreeSet<Integer>> create_nodes(int n){
		TreeMap<Integer, TreeSet<Integer>> tm = new TreeMap<>();
		for (int i = 0; i < n; i++)
			tm.put(i, new TreeSet<Integer>());
		return tm;
	}
	
	public static Scanner vhod;
	
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, InterruptedException {
		vhod = new Scanner(System.in);
		double start = System.currentTimeMillis();
		System.out.println("Vnesite stevilo vozlisc (n): ");
		int n = vhod.nextInt();
		//int n = 100;
		System.out.println("Vnesite verjetnost povezave (p): ");
		double p = vhod.nextDouble();
		//double p = 0.01;
		System.out.println("Vnesite stevilo izhodnih datotek (m): ");
		//int m = 1;
		int m = vhod.nextInt();
		System.out.println("Postopek generacije se zacenja, prosimo pocakajte.");
		vhod.close();
		
		TreeMap<Integer, TreeSet<Integer>> temp_map = make_connections(p, n);
		System.out.println("Generiram grafe. To lahko vzame nekaj minut.");
		while(m>0) {
			while(true) {
					if(is_connected(temp_map)) {
					 break;	
					}
				c++;
				temp_map = make_connections(p, n);
			}
		
			System.out.println(temp_map);
			try {
				System.out.println("Attempts: "+c);
				c = 0;
			} catch (Exception e) {
				System.err.println(e);
			}
			
			writeOut(m, temp_map);
			m--;
			temp_map = make_connections(p, n);
		}
		
		System.out.println((System.currentTimeMillis()-start)/1000);
	}
}
