import os
import networkx as nx

for _,_,file in os.walk("./"):
    for f in file:
        try:
            nodes = []
            if f[-6:]==(".edges"):
                vrstice = open(f, "r").readlines()
                    
            elif f[-4:]==(".mtx"):
                vrstice = open(f, "r").readlines()[2:]
            else:
                continue    
            
            reg = ","
                
            if len(vrstice[0].split(","))==1:
                reg = " "
                
            for vrstica in vrstice:            
                x,y = vrstica.replace("\n","").split(reg)[:2]
                nodes.append( (int(x),int(y)) )      
            G = nx.Graph(nodes)
            #printf(nodes)
            nx.write_sparse6(G, f[:-4 if f[-4:]==(".mtx") else -6 ]+'.s6')
        except:
            print(f)